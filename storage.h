#ifndef STORAGE_H
#define STORAGE_H

#include <QObject>
#include <taskmodel.h>
#include <QFile>
#include <QTextStream>
#include "taskmodel.h"
#include <memory>
class Storage
{
public:
    static void saveData(StackModel &stackModel);
    static StackModel loadData();
private:
    static const QString filename;
};

#endif // STORAGE_H
