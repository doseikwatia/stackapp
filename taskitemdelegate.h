#ifndef TASKITEMDELEGATE_H
#define TASKITEMDELEGATE_H
#include<QItemDelegate>
#include <QSpinBox>
#include <QPainter>
#include <QTreeView>
#include <QProgressBar>
#include <QApplication>
class TaskItemDelegate:public QItemDelegate
{
public:
    TaskItemDelegate(QObject *parent=0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif // TASKITEMDELEGATE_H
