#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->taskView->setContextMenuPolicy(Qt::CustomContextMenu);
    StackModel storageModel=Storage::loadData();
    m_projectListModel= new  StackViewModel(storageModel);
    ui->projectListView->setModel(m_projectListModel);
    connect(m_projectListModel,&QStandardItemModel::dataChanged,this,&MainWindow::hasUnsavedChanges);
    connect(m_projectListModel,&QStandardItemModel::rowsInserted,this,&MainWindow::hasUnsavedChanges);
    ui->actionAdd_Task->setEnabled(false);
    ui->actionRemove_Task->setEnabled(false);
    ui->actionAdd_SubTask->setEnabled(false);
    ui->taskView->setColumnWidth(0,100);
    ui->taskView->setColumnWidth(1,200);
    ui->taskView->setColumnWidth(4,500);
    ui->taskView->setAlternatingRowColors(true);
    ui->taskView->setSortingEnabled(true);
    m_delegate=new TaskItemDelegate(this);
    ui->taskView->setItemDelegate(m_delegate);
    m_taskSortFilterModel= new TaskViewSortFilterModel(this);
    connect(ui->projectListView,&QListView::clicked,this,&MainWindow::projectSelected);
    connect(ui->taskView,&QTreeView::clicked, this, &MainWindow::taskSelected);
    connect(ui->projectListView,&QTreeView::customContextMenuRequested,this,&MainWindow::showTaskViewCustomContextMenu);
    connect(ui->taskTxtFilter,&QLineEdit::textChanged,this,&MainWindow::filterTextChanged);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showNewProjectDialog()
{
    std::unique_ptr<NewProjectDialog>dialog(new NewProjectDialog());
    dialog-> setModal(true);
    if(dialog->exec()==QDialog::Accepted)
    {
        QString projectName=dialog->getProjectName();
        QStandardItem *itm = new QStandardItem(projectName);
        TaskViewModel *childrenModel= new TaskViewModel();
        QVariant childDataModel=QVariant::fromValue(childrenModel);
        itm->setData(childDataModel,StackViewModel::CHILDROLE);
        m_projectListModel->appendRow(itm);
    }

}

void MainWindow::showTaskViewCustomContextMenu(QPoint pos)
{
    QMenu menu(this);
    menu.addAction(QIcon(),"New Project",this,&MainWindow::showNewProjectDialog);
    menu.exec(mapToGlobal(pos));
}

void MainWindow::projectSelected(QModelIndex selectedIndex)
{
    int row=selectedIndex.row();
    int column= selectedIndex.column();
    if(m_currentTaskModel != NULL)
    {
        disconnect(m_currentTaskModel,&TaskViewModel::dataChanged,this,&MainWindow::hasUnsavedChanges);
        disconnect(m_currentTaskModel,&TaskViewModel::rowsRemoved,this,&MainWindow::hasUnsavedChanges);
        disconnect(m_currentTaskModel,&TaskViewModel::rowsInserted,this,&MainWindow::hasUnsavedChanges);
    }
    const QStandardItem* selectedItem= m_projectListModel->item(row,column);
    QVariant childData=selectedItem->data(StackViewModel::CHILDROLE);
    TaskViewModel* childDataModel=childData.value<TaskViewModel*>();
    m_currentTaskModel=childDataModel;
    m_taskSortFilterModel->setSourceModel(m_currentTaskModel);
    ui->taskView->setModel(m_taskSortFilterModel);
    ui->actionAdd_Task->setEnabled(true);
    ui->actionRemove_Task->setEnabled(true);
    connect(m_currentTaskModel,&TaskViewModel::dataChanged,this,&MainWindow::hasUnsavedChanges);
    connect(m_currentTaskModel,&TaskViewModel::rowsRemoved,this,&MainWindow::hasUnsavedChanges);
    connect(m_currentTaskModel,&TaskViewModel::rowsInserted,this,&MainWindow::hasUnsavedChanges);
}

void MainWindow::taskSelected(QModelIndex selectedIndex)
{
    bool enable=selectedIndex.row()>=0;
    ui->actionAdd_SubTask->setEnabled(enable);
    ui->actionRemove_Task->setEnabled(enable);
}

void MainWindow::on_actionExit_triggered()
{
        QApplication::exit();
}

void MainWindow::on_actionSave_triggered()
{
    StackModel stackModel;
    int  numberofprojects=m_projectListModel->rowCount();
    for( int row=0; row <numberofprojects; row++)
    {
        QModelIndex index=m_projectListModel->index(row,0);
        QStandardItem* item=  m_projectListModel->itemFromIndex(index);
        QString projectName= item->data(Qt::DisplayRole).toString();
        TaskViewModel* taskViewModel= item->data(StackViewModel::CHILDROLE).value<TaskViewModel*>();
        QList<TaskModel> taskModels(*taskViewModel);
        ProjectModel projectModel(projectName,taskModels);
        stackModel.addProject(projectModel);
    }
    Storage::saveData(stackModel);
    setWindowTitle(STACKAPP);
}

void MainWindow::on_actionNew_triggered()
{
    this->showNewProjectDialog();
}

void MainWindow::on_actionAdd_Task_triggered()
{
       m_currentTaskModel->createTask();
}

void MainWindow::on_actionAdd_SubTask_triggered()
{
    QModelIndex parentModelIndex=ui->taskView->currentIndex();
    m_currentTaskModel->createSubTask(m_taskSortFilterModel->mapToSource(parentModelIndex));
    ui->taskView->expand(parentModelIndex);
}

void MainWindow::on_actionRemove_Task_triggered()
{
    bool viewIsProjectListView=ui->projectListView->hasFocus();
    QAbstractItemView* view=viewIsProjectListView? ui->projectListView:ui->taskView->hasFocus()?view=ui->taskView:NULL;

    if(view != NULL)
    {
        QModelIndex currentIndex=view->currentIndex();
        QModelIndex targetModelIndex= currentIndex.sibling(currentIndex.row(),0);
        //If target model is a task model with children or if it is a project model show warning message
        if(targetModelIndex.child(0,0).isValid()||targetModelIndex.data(StackViewModel::CHILDROLE).isValid())
        {
            QMessageBox confimationMessageDialog(QMessageBox::Warning,
                                                 "Confirm",
                                                 "Do you want to continue?",
                                                 QMessageBox::No|QMessageBox::Yes);
            confimationMessageDialog.setDefaultButton(QMessageBox::No);
            confimationMessageDialog.setWindowIcon(QIcon(":icons/stack.png"));
            if(confimationMessageDialog.exec()==QMessageBox::No)
                return;
            if(viewIsProjectListView)
                ui->taskView->setModel(NULL);
        }
        view->model()->removeRows(targetModelIndex.row(),1,targetModelIndex.parent());
        QModelIndex newIndex=view->currentIndex();
        bool taskIsSelected=newIndex.isValid();
        ui->actionRemove_Task->setEnabled(taskIsSelected);
        ui->actionAdd_SubTask->setEnabled(taskIsSelected);
        ui->actionAdd_Task->setEnabled(taskIsSelected);
    }

}

void MainWindow::filterTextChanged(const QString &text)
{
    m_taskSortFilterModel->setFilterRegExp(text);
}

void MainWindow::hasUnsavedChanges()
{
    setWindowTitle(STACKAPP+"*");
}
