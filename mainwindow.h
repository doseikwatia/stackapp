#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStandardItemModel>
#include <QMessageBox>
#include "newprojectdialog.h"
#include "taskviewmodel.h"
#include "taskitemdelegate.h"
#include "taskmodel.h"
#include "storage.h"
#include<memory>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void showNewProjectDialog();
    void showTaskViewCustomContextMenu(QPoint);
    void projectSelected(QModelIndex selectedIndex);
    void taskSelected(QModelIndex selectedIndex);
    void on_actionExit_triggered();
    void on_actionSave_triggered();
    void on_actionNew_triggered();
    void on_actionAdd_Task_triggered();
    void on_actionAdd_SubTask_triggered();
    void on_actionRemove_Task_triggered();
    void filterTextChanged(const QString &text);
    void hasUnsavedChanges();
private:
    Ui::MainWindow *ui; 
    QStandardItemModel *m_projectListModel=NULL;
    TaskViewModel *m_currentTaskModel=NULL;
    TaskViewSortFilterModel *m_taskSortFilterModel=NULL;
    TaskItemDelegate *m_delegate=NULL;
    const QString STACKAPP="StackApp";
};

#endif // MAINWINDOW_H
