#ifndef TASKVIEWMODEL_H
#define TASKVIEWMODEL_H
#include<QStandardItemModel>
#include<QDateTime>
#include "taskmodel.h"
#include <QVariant>
#include <QSortFilterProxyModel>
class TaskViewModel:public QStandardItemModel
{
    Q_OBJECT


public:
    TaskViewModel(QObject* parent=0);
    TaskViewModel(const QList<TaskModel>& storageModel);
    void createSubTask(QModelIndex &parent);
    void createTask();
    QList<QStandardItem*> GetItemsAtRow(int row, QStandardItem* parentItem) const;
    TaskModel GetTaskModel(QList<QStandardItem*> items, TaskModel *parentTask=0) const;
    operator QList<TaskModel>() const;
    void InitializeHeaders();
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent);
    QMimeData *mimeData(const QModelIndexList &indexes) const;
    void updateParentTaskProgress(QModelIndex &parentIndex);
    bool removeRows(int row, int count, const QModelIndex &parent);
    bool insertRows(int row, int count, const QModelIndex &parent);
private:
    void BuildViewModel(const QList<TaskModel> &storageModel, QStandardItem *parent=NULL);

    QList<QStandardItem*> createTaskRow(TaskModel taskModel);
    QList<QStandardItem *> createTaskRow(char status=0, QString title="Title", QDate duedate=QDate::currentDate(), char priority=0, QString description="Description");
    void updateTaskProgress(const QModelIndex &index, const QVariant &data);
};
class StackViewModel:public QStandardItemModel
{
public:
    StackViewModel(const StackModel &storageModel);
    static const int CHILDROLE;

};
class TaskViewSortFilterModel:public  QSortFilterProxyModel
{ 
public :
    explicit TaskViewSortFilterModel(QObject *parent=0);
protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
    bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const;
};

#endif // TASKVIEWMODEL_H
