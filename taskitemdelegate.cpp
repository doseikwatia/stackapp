#include "taskitemdelegate.h"

TaskItemDelegate::TaskItemDelegate(QObject *parent):
    QItemDelegate(parent)
{

}

QWidget *TaskItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    int column=index.column();
    QWidget *result;
    if(column==0)
    {
        result=new QSpinBox(parent);
    }
    else
        result = QItemDelegate::createEditor(parent, option, index);
    return result;
}

void TaskItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    int column=index.column();
    if(column==0)
    {
        QSpinBox *spinbox=dynamic_cast<QSpinBox*>(editor);
        spinbox->setRange(0,100);
        int data=index.data().toInt();
        spinbox->setValue(data);
        bool hasChildren=index.model()->hasChildren(index);
        spinbox->setReadOnly(hasChildren);
    }
    else if(column==3)
    {
        QSpinBox *spinbox=dynamic_cast<QSpinBox*>(editor);
        spinbox->setRange(0,10);
        int data=index.data().toInt();
        spinbox->setValue(data);
    }
    else
        QItemDelegate::setEditorData(editor,index);
}


void TaskItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    int column= index.column();
    if(column==0)
    {
        painter->save();
        if (option.state & QStyle::State_Selected)
            painter->fillRect(option.rect, option.palette.highlight());
        QStyle *style=QApplication::style();
        QStyleOptionProgressBar opt;
        opt.minimum=0;
        opt.maximum=100;
        opt.progress=index.data().toInt();
        opt.rect=option.rect;
        style->drawControl(QStyle::CE_ProgressBar,&opt,painter);
        //painter->end();
        //progressBar.render(painter,option.rect.topLeft(),QRegion(option.rect));
        //painter->begin(device);
//        QRect rect=option.rect;
//        rect.setWidth(rect.width()-20);
//        QPoint startPoint=rect.topLeft();
//        int percentComplete=index.data().toInt();
//        painter->setPen(QPen(Qt::black));
//        painter->setBrush(QBrush(Qt::white));
//        painter->drawRect(rect);
//        int green=static_cast<double>(percentComplete)*255/100;
//        QColor color((255-green),green,0);
//        painter->setBrush(QBrush(color));
//        int newWidth=percentComplete*rect.width()/100;
//        painter->drawRect(startPoint.x(),startPoint.y(),newWidth,rect.height());
        painter->restore();
    }
    else
        QItemDelegate::paint(painter, option,index);
}


