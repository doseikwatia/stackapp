#ifndef NEWPROJECTDIALOG_H
#define NEWPROJECTDIALOG_H

#include <QDialog>
#include <QPushButton>
#include "ui_newprojectdialog.h"
namespace Ui {
class NewProjectDialog;
}

class NewProjectDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewProjectDialog(QWidget *parent = 0) :
        QDialog(parent),
        ui(new Ui::NewProjectDialog)
    {
        ui->setupUi(this);
        this->projectNameChanged("");
        connect(ui->projectNameEdtr,SIGNAL(textChanged(QString)),this,SLOT(projectNameChanged(QString)));
        this->setWindowTitle("New Project");
    }
    QString getProjectName() const
    {
        return ui->projectNameEdtr->text();
    }
    ~NewProjectDialog()
    {
        delete ui;
    }

public slots:
    void projectNameChanged(QString newProjectName)
    {
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(!newProjectName.isEmpty());
    }

private:
    Ui::NewProjectDialog *ui;
};



#endif // NEWPROJECTDIALOG_H
