#ifndef TASKMODEL_H
#define TASKMODEL_H

#include <QObject>
#include <QDateTime>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QVariant>
#include <QJsonDocument>
class TaskModel
{
public:
    explicit TaskModel(quint8 status, QString title, QString description, QDate duedate, quint8 priority, TaskModel *parent = 0);
    TaskModel(QJsonObject json);
    TaskModel (const TaskModel &taskModel);
    QString title() const;
    void setTitle(const QString &title);

    QString description() const;
    void setDescription(const QString &description);

    QDate duedate() const;
    void setDuedate(const QDate &duedate);

    QList<TaskModel> children() const;

    TaskModel *parent() const;
    void setParent(TaskModel *parent);
    operator QJsonObject() const;
    void addChild(TaskModel child);

    char status() const;
    void setStatus(const char& status);

    char priority() const;
    void setPriority(const char &priority);

private:
    QString m_title;
    QString m_description;
    quint8 m_status;
    quint8 m_priority;
    QDate m_duedate;
    QList<TaskModel> m_children;
    TaskModel *m_parent;
    const QString TITLE ="Title";
    const QString STATUS="Status";
    const QString DUEDATE="DueDate";
    const QString DESCRIPTION="Description";
    const QString CHILDREN="Children";
    const QString PRIORITY="Priority";
};
class ProjectModel
{
public:
    ProjectModel();
    ProjectModel(QJsonObject json);
    ProjectModel(QString projectName, QList<TaskModel> tasks);
    operator QJsonObject() const;
    QString projectName() const;
    void setProjectName(const QString &projectName);
    QList<TaskModel> tasks() const;
    void setTasks(QList<TaskModel> tasks);

private:
    QList<TaskModel> m_tasks;
    QString m_projectName;
    const QString TASK="Task";
    const QString PROJECT="Project";
};
class StackModel
{
public :
    StackModel();
    StackModel(QJsonObject json);
    operator QJsonObject() const;

    QList<ProjectModel> projects() const;
    void setProjects(const QList<ProjectModel> &projects);
    void addProject(ProjectModel project);
private:
    const QString PROJECTS="Projects";
    QList<ProjectModel> m_projects;
};
#endif // TASKMODEL_H
