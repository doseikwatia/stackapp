#include "taskmodel.h"

TaskModel::TaskModel(quint8 status, QString title, QString description, QDate duedate, quint8 priority, TaskModel *parent)
{
    m_status=status;
    m_title=title;
    m_description=description;
    m_duedate=duedate;
    m_parent=parent;
    m_priority=priority;
}

TaskModel::TaskModel(QJsonObject json)
{
    m_title=json[TITLE].toString();
    m_description= json[DESCRIPTION].toString();
    m_duedate =json[DUEDATE].toVariant().toDate();
    m_priority=json[PRIORITY].toInt();
    m_status=json[STATUS].toInt();
    QJsonArray childrenArray=json[CHILDREN].toArray();
    foreach(const QJsonValue &value, childrenArray)
    {
        QJsonObject obj= value.toObject();
        m_children.append(TaskModel(obj));
    }

}

TaskModel::TaskModel(const TaskModel &taskModel)
{
    m_children =QList<TaskModel>(taskModel.children());
    m_title=taskModel.title();
    m_duedate=taskModel.duedate();
    m_description=taskModel.description();
    m_parent=taskModel.parent();
    m_status=taskModel.status();
    m_priority=taskModel.priority();
}

QString TaskModel::title() const
{
    return m_title;
}

void TaskModel::setTitle(const QString &title)
{
    m_title = title;
}

QString TaskModel::description() const
{
    return m_description;
}

void TaskModel::setDescription(const QString &description)
{
    m_description = description;
}

QDate TaskModel::duedate() const
{
    return m_duedate;
}

void TaskModel::setDuedate(const QDate &duedate)
{
    m_duedate = duedate;
}

QList<TaskModel> TaskModel::children() const
{
    return m_children;
}

TaskModel *TaskModel::parent() const
{
    return m_parent;
}

void TaskModel::setParent(TaskModel *parent)
{
    m_parent = parent;
}

void TaskModel::addChild(TaskModel child)
{
    m_children.append(child);
}

char TaskModel::status() const
{
    return m_status;
}

void TaskModel::setStatus(const char &status)
{
    m_status = status;
}

char TaskModel::priority() const
{
    return m_priority;
}

void TaskModel::setPriority(const char &priority)
{
    m_priority = priority;
}

TaskModel::operator QJsonObject() const
{
    QJsonObject result;
    result [TITLE]=m_title;
    result [STATUS]= m_status;
    result [DUEDATE]= QJsonValue::fromVariant( QVariant::fromValue(m_duedate));
    result [DESCRIPTION]= m_description;
    result [PRIORITY]=m_priority;
    QJsonArray children;
    QJsonObject child;
    foreach (child, m_children) {
        children.append(child);
    }
    result [CHILDREN]= children;
    return result;
}

ProjectModel::ProjectModel()
{

}

ProjectModel::ProjectModel(QJsonObject json)
{
    m_projectName=json[PROJECT].toString();
    QJsonArray taskArray=json[TASK].toArray();
    foreach (const QJsonValue &value, taskArray) {
        QJsonObject obj = value.toObject();
        m_tasks.append(TaskModel(obj));
    }
}

ProjectModel::ProjectModel(QString projectName, QList<TaskModel> tasks)
{
    m_projectName=projectName;
    m_tasks=tasks;
}

QString ProjectModel::projectName() const
{
    return m_projectName;
}

void ProjectModel::setProjectName(const QString &projectName)
{
    m_projectName = projectName;
}

QList<TaskModel> ProjectModel::tasks() const
{
    return m_tasks;
}

void ProjectModel::setTasks(QList<TaskModel> tasks)
{
    m_tasks = tasks;
}

ProjectModel::operator QJsonObject() const
{
    QJsonObject result;
    result[PROJECT]=m_projectName;
    QJsonArray taskJsonArray;
    foreach (const TaskModel task, m_tasks) {
        taskJsonArray.append(QJsonValue(task));
    }
    result[TASK]=taskJsonArray;
    return result;
}

StackModel::StackModel()
{

}

StackModel::StackModel(QJsonObject json)
{
    QJsonArray projectArray=json[PROJECTS].toArray();
    foreach(const QJsonValue &value, projectArray)
    {
        QJsonObject obj=value.toObject();
        m_projects.append(ProjectModel(obj));
    }
}

QList<ProjectModel> StackModel::projects() const
{
    return m_projects;
}

void StackModel::setProjects(const QList<ProjectModel> &projects)
{
    m_projects = projects;
}

void StackModel::addProject(ProjectModel project)
{
    m_projects.append(project);
}

StackModel::operator QJsonObject() const
{
    QJsonArray jsonArray;
    foreach(const ProjectModel & pjtModel, m_projects)
    {
        QJsonValue jsonVal(pjtModel);
        jsonArray.append(jsonVal);
    }
    QJsonObject result;
    result [PROJECTS]=jsonArray;
    return result;
}
