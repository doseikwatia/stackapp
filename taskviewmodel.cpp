#include "taskviewmodel.h"

TaskViewModel::TaskViewModel(QObject *parent):
    QStandardItemModel(parent)
{
    InitializeHeaders();
}

TaskViewModel::TaskViewModel(const QList<TaskModel> &storageModel)
{
    InitializeHeaders();
    BuildViewModel(storageModel,invisibleRootItem());
}
void TaskViewModel::InitializeHeaders()
{
    QStringList headerList={"Status","Title","Due Date","Priority","Description"};
    this->setHorizontalHeaderLabels(headerList);
}

QList<QStandardItem *> TaskViewModel::createTaskRow(char status, QString title, QDate duedate, char priority, QString description)
{
    QDate today=QDate::currentDate();
    QList<QStandardItem*> row={
        new QStandardItem(QString("%1").arg(static_cast<int>(status))),
        new QStandardItem(title),
                                  new QStandardItem(duedate.toString()),
                                  new QStandardItem(QString("%1").arg(static_cast<int>(priority))),
                                  new QStandardItem(description)
    };
    row.at(0)->setData(QVariant::fromValue(static_cast<int>(status)), Qt::EditRole);
    row.at(2)->setData(QVariant::fromValue(today),Qt::EditRole);
    row.at(3)->setData(QVariant::fromValue(static_cast<int>(priority)),Qt::EditRole);
    return row;
}

void TaskViewModel::createTask()
{
    QList<QStandardItem*> row = createTaskRow();
    this->appendRow(row);
}

QList<QStandardItem *> TaskViewModel::GetItemsAtRow(int row, QStandardItem *parentItem) const
{
    QList<QStandardItem*> result;
    const int numberofColumns=columnCount();

    for(int col =0; col< numberofColumns; col++)
    {
        result.append(parentItem->child(row,col));
    }
    return result;
}

TaskModel TaskViewModel::GetTaskModel(QList<QStandardItem *> items, TaskModel* parentTask) const
{
    int status=0;
    QString title;
    QString description;
    int priority=0;
    QDate duedate;
    foreach(QStandardItem* item, items)
    {
        int column=item->column();
        QVariant data=item->data(Qt::EditRole);
        switch (column)
        {
        case 0:
            status=data.toInt();
            break;
        case 1:
            title=data.toString();
            break;
        case 2:
            duedate=data.toDate();
            break;
        case 3:
            priority=data.toInt();
            break;
        case 4:
            description=data.toString();
            break;
        }
    }
    TaskModel  taskModel(status,title,description,duedate,priority,parentTask);

    if(items[0]->hasChildren())
    {
        int idx=0;
        QList<QStandardItem*> childRow;
        do
        {
            childRow= GetItemsAtRow(idx++,items[0]);
            if(childRow[0]==0)
                continue;
            taskModel.addChild( GetTaskModel(childRow,&taskModel));

        }while(childRow[0]!=0);
    }
    return taskModel;
}

TaskViewModel::operator QList<TaskModel>() const
{
    QList<TaskModel> result;
    QStandardItem * rootItem=this->invisibleRootItem();
    for(int row =0; row<rowCount();row++)
    {
        QList<QStandardItem*> items=GetItemsAtRow(row,rootItem);
        result << GetTaskModel(items);
    }
    return result;
}

void TaskViewModel::BuildViewModel(const QList<TaskModel> &storageModel, QStandardItem *parent)
{
    foreach(TaskModel model, storageModel)
    {
        QList<QStandardItem*> row= createTaskRow(model);
        parent->appendRow(row);
        BuildViewModel(model.children(),row[0]);
    }
}

QList<QStandardItem *> TaskViewModel::createTaskRow(TaskModel taskModel)
{
    char status=taskModel.status();
    QString title = taskModel.title();
    char priority= taskModel.priority();
    QString description=taskModel.description();
    QDate duedate=taskModel.duedate();
    QList<QStandardItem*> result = createTaskRow(status,title,duedate,priority,description);
    return result;
}

bool TaskViewModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(index.isValid() && index.column()==0)
        updateTaskProgress(index,value);
    bool result = QStandardItemModel::setData(index,value,role);
    return result;
}

void TaskViewModel::updateParentTaskProgress(QModelIndex& parentIndex)
{
    if(!parentIndex.isValid())
        return;
    QModelIndex childIndex=parentIndex.child(0,0);
    if(!childIndex.isValid())
        return;
    QVariant childData=childIndex.data();
    updateTaskProgress(childIndex,childData);
}

bool TaskViewModel::removeRows(int row, int count, const QModelIndex &parent)
{
    bool successfullyRemoved = QStandardItemModel::removeRows(row, count, parent);
    if(successfullyRemoved)
    {
        updateParentTaskProgress(QModelIndex(parent));
    }
    return successfullyRemoved;
}

bool TaskViewModel::insertRows(int row, int count, const QModelIndex &parent)
{
    bool result =QStandardItemModel::insertRows(row,count,parent);
    updateParentTaskProgress(QModelIndex(parent));
    return result;
}

bool TaskViewModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    QModelIndex parentIndex=parent.isValid() && parent.column()==0?parent:parent.sibling(parent.row(),0);
    bool result =QStandardItemModel::dropMimeData(data,action,row,column,parentIndex);
    return result;
}

QMimeData *TaskViewModel::mimeData(const QModelIndexList &indexes) const
{
    QModelIndexList indices;
    QModelIndex firstIndex=indexes[0];
    int row=firstIndex.row();
    for(int col =0;col<columnCount();col ++)
    {
        indices.append(firstIndex.sibling(row,col));
    }
    QMimeData *result=QStandardItemModel::mimeData(indices);
    return result;
}

void TaskViewModel::createSubTask(QModelIndex &parent)
{
    QModelIndex parentModelIndex =parent.sibling(parent.row(),0);
    if(!parentModelIndex.isValid())
        return;
    QList<QStandardItem*> row = createTaskRow();
    QStandardItem* item= this->itemFromIndex(parentModelIndex);
    item->appendRow(row);
    updateParentTaskProgress(parentModelIndex);
}
void TaskViewModel::updateTaskProgress(const QModelIndex &index, const QVariant &data)
{
    QStandardItemModel::setData(index,data);
    QModelIndex parentIndex=this->parent(index);
    if(!parentIndex.isValid())
        return;
    QModelIndex siblingIndex;
    int siblingProgress=0;
    int siblingRowIndex=0;
    int totalProgress=0;
    while(true)
    {
        siblingIndex=index.sibling(siblingRowIndex++,index.column());
        if(!siblingIndex.isValid())
            break;
        siblingProgress+=siblingIndex.data().toInt();
        totalProgress+=100;
    }
    int parentProgress=siblingProgress*100/totalProgress;

    updateTaskProgress(parentIndex,QVariant::fromValue(parentProgress));

}


StackViewModel::StackViewModel(const StackModel &storageModel)
{
    QList<ProjectModel>projectModels=storageModel.projects();
    foreach(ProjectModel projectModel, projectModels)
    {
        QStandardItem *projectItem =new QStandardItem(projectModel.projectName());
        this->appendRow(projectItem);
        projectItem->setData(QVariant::fromValue(new TaskViewModel(projectModel.tasks())),StackViewModel::CHILDROLE);
    }
}

const int StackViewModel::CHILDROLE=Qt::UserRole+1;

TaskViewSortFilterModel::TaskViewSortFilterModel(QObject *parent):
    QSortFilterProxyModel(parent)
{

}

bool TaskViewSortFilterModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    bool result = (sourceModel()->index(source_row,1,source_parent).data().value<QString>()+
                   sourceModel()->index(source_row,4,source_parent).data().value<QString>()).contains(filterRegExp().pattern(),Qt::CaseInsensitive);
    return result;
}

bool TaskViewSortFilterModel::lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const
{
    bool result =false;
    QVariant left=source_left.data(filterRole());
    QVariant right= source_right.data(filterRole());
    if(left.canConvert(QVariant::Int))
        result = left.value<int>()<right.value<int>();
    else if (left.canConvert(QVariant::String))
        result = left.value<QString>()<right.value<QString>();
    else if (left.canConvert(QVariant::Date))
        result = left.value<QDate>()<right.value<QDate>();
    return result;
}


