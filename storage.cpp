#include "storage.h"

StackModel Storage::loadData()
{
    std::unique_ptr<StackModel> result(new StackModel());
    QFile datafile(filename);
    if(!datafile.open(QIODevice::ReadOnly))
        return *result;
    QTextStream stream(&datafile);
    QString  jsonString=stream.readAll();
    QJsonDocument document=QJsonDocument::fromJson(jsonString.toUtf8());
    QJsonObject documentObj=document.object();
    result.reset(new StackModel(documentObj));
    return *result;
}

void Storage::saveData(StackModel& stackModel)
{
    QFile datafile(filename);
    if(!datafile.open(QIODevice::WriteOnly))
        return;
    QTextStream stream(&datafile);
    QJsonObject stackModelJson=stackModel;
    QJsonDocument document(stackModelJson);
    QByteArray documentByteArr=document.toJson();
    stream <<QString(documentByteArr);
    datafile.flush();
}

const QString Storage::filename="StackApp.json";
